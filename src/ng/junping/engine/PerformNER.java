package ng.junping.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import ng.junping.classifier.OpenNLPNER;

/**
 * Entry class which takes in input data, and invokes our wrapper for OpenNLP's NER 
 *  engine, and outputs the named entities found inside the input text
 * This engine is implemented as a Singleton to avoid repeatedly loading the 
 *  underlying OpenNLP models.
 *  
 * @author junping
 *
 */
public class PerformNER {

	/**
	 * Holds a reference to the singleton object instance
	 */
	private volatile static PerformNER singleton = null;
		
	/**
	 * Wrapper to underlying OpenNLP NER engine that we will invoke. 
	 */
	private OpenNLPNER engineNER;
	
	
	/**
	 * Constructor is made private to enforce Singleton pattern
	 */
	private PerformNER() {
	
		// Initialize fields
		
		// Starts up the OpenNLP NER engine, loading up the required model
		// This gets us ready to handle queries when required
		// This can potentially be null
		engineNER = OpenNLPNER.getSingletonInstance();
		
		
	} // end constructor
	
	/**
	 * The accessor function to call to get an instance of this NER engine
	 * This method is thread-safe.
	 * @return A singleton instance of this NER engine
	 */
	public static synchronized PerformNER getSingletonInstance() {
		if (singleton == null) {
			singleton = new PerformNER();
		}
		return singleton;
	} // end getSingletonInstance
	
	
	/**
	 * Identifies the named entities found in an input sentence
	 * @return ArrayList containing every named entity found within sentence. Name entities 
	 *    are in the format X, where X is an actual string found in the sentence.
	 *    This function returns null on any error conditions.
	 */
	public ArrayList<String> getNamedEntities(String inputStr) {
		
		// Sanity check
		if (engineNER == null) {
			return null;
		}
		
		
		// Start processing input
					
		// Our NER wrapper is thread-safe
		ArrayList<String> namedEntities = null;		
		namedEntities = engineNER.getNamedEntities(inputStr);
				
		// Return generated results
		return namedEntities;
		
	} // end getNamedEntities()
	
	/**
	 * Entry function that starts the NER tagging process
	 * @param args Input arguments - none expected as of time of writing (7Aug2014)
	 */
	public static void main(String[] args) {
		
		// Create instance of engine
		PerformNER engineNER = PerformNER.getSingletonInstance();
		
		// Reads input sentences from stdin to be processed.
		// Output sent to stdout
		try {
			
			// A more elaborate interface to request for user input and to display output would 
			//   be good, but this is sufficient to demonstrate the functionalities of this NER
			//   engine. 
			BufferedReader stdinReader = new BufferedReader(new InputStreamReader(System.in));
			String inputStr;
			while ((inputStr = stdinReader.readLine()) != null) {
				
				// Invoke engine
				ArrayList<String> namedEntities = engineNER.getNamedEntities(inputStr);
				if (namedEntities == null) {
					// Unable to get output, prompt user for retry
					// Depending on the situation, it may also be appropriate to exit 
					System.err.println("Unable to get named entities. Please retry.");
					continue;
				}
				
				// Display output
				for (String namedEntity : namedEntities) {
					System.out.println("-> " + namedEntity);
				} // end for namedEntity...
				
			} // end while
			
		} catch (IOException ex) {
			
			// Unexpected error, inform user and quit program
			System.err.println("An error has occured. Detail of error: " + ex.toString());
			System.err.println("This program will now terminate.");
			
		} // end try-catch (IOException)
 
	} // end main()

} // end class PerformNER
