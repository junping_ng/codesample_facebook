package ng.junping.classifier;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.util.Span;

/**
 * Wrapper class over Apache OpenNLP's NER engine
 * Implemented as a singleton as we should only expect one such instance
 * @author junping
 *
 */
public class OpenNLPNER {

	/**
	 * Singleton instance 
	 */
	private volatile static OpenNLPNER singleton = null;
	
	/**
	 * Object used to ensure sequential access to underlying NER engine.
	 * The OpenNLP name finder engine is not thread-safe
	 */
	private Object requestLock;
	
	/**
	 * Holds input model file
	 */
	private InputStream modelIn;
	
	/**
	 * Model for NER engine
	 */
	private TokenNameFinderModel model;
	
	/**
	 * Actual NER engine
	 */ 
	private NameFinderME maxentNameFinder;
	
	
	/**
	 * Private constructor, to help implement the singleton pattern
	 */
	private OpenNLPNER() {
		
	} // end constructor
	
	
	public static synchronized OpenNLPNER getSingletonInstance() {
		
		if (singleton == null) {
			singleton = new OpenNLPNER();
			try {
				singleton.initialize();
			} catch (Exception ex) {
				// Error during initialization, we cannot proceed
				singleton = null;
			}
		}
		return singleton;
		
	} // end getSingletonInstance()
	
	
	/**
	 * Initializes the underlying NER engine
	 * Should only be called by ourselves, thus declared private.
	 * @throws Exception in the event of any errors. If an exception is thrown, this class must 
	 *    not be used anymore
	 */
	private void initialize() throws Exception {
		
		// Multiple stages involved in loading engine
		// Exceptions are thrown on errors
		// We try to clean up as much as we can
		
		// 0. Preliminaries
		requestLock = new Object();
		
		// 1. Read model file
		try {
			
			modelIn = new FileInputStream("resources/opennlp-models/en-ner-person.bin");
			model = new TokenNameFinderModel(modelIn);
			
		} catch (IOException ex) {
			
			if (modelIn != null) {
				try {
					modelIn.close();
				} catch (IOException exInner) {
					// We can ignore this, nothing much we can do if the close fails
				}
				modelIn = null;
			}
			// Stop initialization
			throw new Exception("Unable to read in model.");
			
		} // end try-catch		
		 
		// 2. Instantiate the actual engine
		maxentNameFinder = new NameFinderME(model);	
				
	} // end initialize()
	

	/**
	 * Returns an array list of named entities found within input sentence.
	 * This function is thread-safe, although the underlying NER engine by
	 *  Apache OpenNLP is not.
	 * @param inputSentence the input sentence we want to process
	 * @return ArrayList containing named entities
	 */
	public ArrayList<String> getNamedEntities(String inputSentence) {
		 
		// Break sentence up into array of word tokens
		String[] sentenceTokens = inputSentence.split(" ");
		
		// Find named entities
		Span[] namedEntities = null;
		synchronized(requestLock) {
			
			try {
			// Make call
			namedEntities = maxentNameFinder.find(sentenceTokens);							
			// Required call by OpenNLP documentation 
			maxentNameFinder.clearAdaptiveData();
			} catch (Exception ex) {
				// In case of errors from external library
				// Log to logger...
			} // end try-catch
			
		} // end synchronized(requestLock...
		
		// Build up return structure
		ArrayList<String> results = new ArrayList<String>();
		if (namedEntities == null) {
			// Can't proceed if we have no results
			return results;
		}
		for (Span namedEntity : namedEntities) {
			// Span is contains the start and ending indices of word tokens belonging to
			//   one instance of an named entity	
			try {
				String person = "";
				for (int i = namedEntity.getStart(); i < namedEntity.getEnd(); ++i) {
					if (i != namedEntity.getStart()) {
						person += " "; // Add in space delimiter for all except first token
					}
					person += sentenceTokens[i];
				}
				// Store the retrieved name
				results.add(person);
			} catch (ArrayIndexOutOfBoundsException ex) {
				// Catches de-reference errors when looking up named entity strings from each Span
				// We have to skip this instance, no choice
			}
		}
		
		return results;
		
	} // end getNamedEntities()
	
} // end class OpenNLPNER
